.. note::

    This is the main page of your project's Sphinx_ documentation. It is formatted in reStructuredText_. Add additional pages by creating rst-files in ``docs`` and adding them to the toctree_ below. Use then references_ in order to link them from this page, e.g. :ref:`authors <authors>` and :ref:`changes`.

    It is also possible to refer to the documentation of other Python packages with the `Python domain syntax`_. By default you can reference the documentation of `Sphinx,`_ Python_, `NumPy,`_ SciPy_, matplotlib_, Pandas_, Scikit-Learn_. You can add more by extending the ``intersphinx_mapping`` in your Sphinx's ``conf.py``.

    The pretty useful extension autodoc_ is activated by default and lets you include documentation from docstrings. Docstrings can be written in Google_ (recommended!), NumPy_ and classical_
    style.
