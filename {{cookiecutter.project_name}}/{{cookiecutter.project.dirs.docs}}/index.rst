=======================================================================
{{ cookiecutter.project_name }}'s Documentation
=======================================================================

This is the documentation of **{{ cookiecutter.project_name }}**.

.. include:: note.rst

.. include:: ../README.rst
  :start-after: readme_description
  :end-before: readme_description_

.. include:: ../README.rst
  :start-after: readme_usage
  :end-before: readme_usage_

.. include:: ../README.rst
  :start-after: readme_features
  :end-before: readme_features_


Contents
========

.. toctree::
   :maxdepth: 2

   Installation <installation>
   Changelog <changes>
   Authors <authors>
   License <license>
   Module Reference <api/modules>


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`



.. _Sphinx: http://sphinx-doc.org/
.. _reStructuredText: http://sphinx-doc.org/rest.html
.. _toctree: http://sphinx-doc.org/markup/toctree.html
.. _references: http://sphinx-doc.org/markup/inline.html

.. _Python domain syntax: http://sphinx-doc.org/domains.html#the-python-domain
.. _Sphinx,: http://sphinx.pocoo.org
.. _Python: http://docs.python.org
.. _NumPy,: http://docs.scipy.org/doc/numpy
.. _SciPy: http://docs.scipy.org/doc/scipy/reference
.. _matplotlib: http://matplotlib.sourceforge.net
.. _Pandas: http://pandas.pydata.org/pandas-docs/stable
.. _Scikit-Learn: http://scikit-learn.org/stable
.. _autodoc: http://www.sphinx-doc.org/en/stable/ext/autodoc.html
.. _Google: http://google.github.io/styleguide/pyguide.html#Comments
.. _NumPy: https://github.com/numpy/numpy/blob/master/doc/HOWTO_DOCUMENT.rst.txt
.. _classical: http://www.sphinx-doc.org/en/stable/domains.html#info-field-lists
