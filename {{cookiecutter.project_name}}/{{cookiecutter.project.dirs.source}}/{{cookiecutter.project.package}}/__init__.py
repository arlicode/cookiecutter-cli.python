# -*- coding: utf-8 -*-
# {{ cookiecutter.project.license.copyright.o }}
# {{ cookiecutter.project.license.copyright.l }}
"""Define package info."""
import logging

import pkg_resources


try:
    __version__ = pkg_resources.get_distribution(__name__).version
except pkg_resources.DistributionNotFound:
    __version__ = 'unknown'

try:
    __location__ = pkg_resources.get_distribution(__name__).location
except pkg_resources.DistributionNotFound:
    __location__ = 'unknown'

try:
    __pkgname__ = pkg_resources.get_distribution(__name__).project_name
except pkg_resources.DistributionNotFound:
    __pkgname__ = 'unknown'

logger = logging.getLogger(__pkgname__)
