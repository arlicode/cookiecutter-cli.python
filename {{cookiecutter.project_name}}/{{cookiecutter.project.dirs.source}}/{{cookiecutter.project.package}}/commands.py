# -*- coding: utf-8 -*-
# {{ cookiecutter.project.license.copyright.o }}
# {{ cookiecutter.project.license.copyright.l }}
"""Subcommands."""
from defscli.base import strdict

from . import logger


def comparser(parser=None, common=None):
    """Parse arguments function.

    Args:
        parser (obj): argparse namespace
        common (obj): argparse namespace

    Returns:
        obj: argparse namespace

    """
    subparsers = parser.add_subparsers(
        title='subcommands')

    parser_data = subparsers.add_parser(
        'data',
        parents=[common],
        help='show data')
    parser_data.set_defaults(
        command=data)

    return


def data(data: object, config: object) -> None:
    """Show data.

    Args:
        data : execution data
        config : config data

    """
    logger.info('ok')

    string = data
    try:
        string = strdict(data)
    except Exception as e:
        logger.debug(f'e : {e}')

    msg = f'data : {string}'
    print(msg)

    logger.debug(msg)
