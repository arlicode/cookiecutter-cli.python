# -*- coding: utf-8 -*-
# {{ cookiecutter.project.license.copyright.o }}
# {{ cookiecutter.project.license.copyright.l }}
"""Core workflow functions."""
from . import logger


def core(config: object) -> None:
    """Process config data and execute command.

    Args:
        config : config data

    """
    logger.info('ok')

    if config:
        command = config.core.command
        data = config.data

        if command:
            command(data, config)
