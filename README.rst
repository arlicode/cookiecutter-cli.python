=======================================================================
Cookiecutter Cli Template
=======================================================================

A cookiecutter_ template to generate a command line interface project
with click for python version >= 3.6.


Usage
-----

Generate the python project

.. code-block:: console

    cookiecutter https://bitbucket.org/arlicode/cookiecutter-cli.python.git

Answer the questions to personalize the project


Features
--------

* Testing with ``py.test`` or ``python setup.py pytest``
* Documentation generation with Sphinx_


Installation
------------

Install cookiecutter_ packages using pip

.. code-block:: console

    pip3 install --user cookiecutter


.. History
.. -------


.. Credits
.. -------


License
-------
BSD


.. _cookiecutter: https://github.com/audreyr/cookiecutter
.. _Pypackage: https://github.com/audreyr/cookiecutter-pypackage
.. _Sphinx: http://sphinx-doc.org/
