# -*- coding: utf-8 -*-
# {{ cookiecutter.project.license.copyright.o }}
# {{ cookiecutter.project.license.copyright.l }}
"""Finish project folder setup."""
from __future__ import print_function
from __future__ import unicode_literals

import glob
import os
import shutil
from pathlib import Path


class FileInfo():
    """Define file information."""
    def __init__(self, file):
        self._file = file
        # extensions not to be considered as code but as jinja2
        # will be ignored by linters and only for coding purposes
        self.legacy = '.legacy'
        self.update = '.update'

    @property
    def path(self):
        """Evaluate given file for valid path."""
        if os.path.isabs(os.path.dirname(self._file)):
            return self._file
        else:
            return os.path.join(os.getcwd(), self._file)

    @property
    def _parts(self):
        """List of basename parts"""
        return [*os.path.basename(self.path).split('.')]

    @property
    def _suffixes(self):
        """List of filename suffixes"""
        return [*self._parts[1:]]

    @property
    def suffix(self):
        """Filename suffix"""
        return '.{}'.format(self._parts[-1])

    @property
    def basename(self):
        """Evaluate suffixes to preserve."""
        basename = '{}.{}'.format(self._parts[0], '.'.join(self._suffixes))
        if self._parts[0] == '':
            basename = '.{}'.format(self._parts[1])
            # suffixes = [*self._parts[1:]]
        if self.suffix in {self.legacy, self.update}:
            basename = '{}.{}'.format(
                self._parts[0], '.'.join(self._suffixes[:-1]))
            if basename[-1] == '.':
                basename = basename[:-1]
            basename = os.path.join(
                os.path.dirname(self.path), basename)
        else:
            basename = self.path
        return basename


def rename_files():
    """Rename template files."""
    cwd = os.getcwd()
    msgn = '      '
    msgr = ''
    msgl = msgn
    files = [
        *glob.glob(
            os.path.join(
                cwd, '{{ cookiecutter.project.dirs.source }}', '*.*')),
        *glob.glob(
            os.path.join(
                cwd, '{{ cookiecutter.project.dirs.source }}', '**', '*.*')),
        *glob.glob(
            os.path.join(
                cwd, '{{ cookiecutter.project.dirs.docs }}', '*.*')),
        *glob.glob(
            os.path.join(
                cwd, '{{ cookiecutter.project.dirs.docs }}', '**', '*.*')),
        *glob.glob(
            os.path.join(
                cwd, '{{ cookiecutter.project.dirs.tests }}', '*.*')),
        *glob.glob(
            os.path.join(
                cwd, '{{ cookiecutter.project.dirs.tests }}', '**', '*.*')),
        *glob.glob(
            os.path.join(
                cwd, '{{ cookiecutter.project.dirs.examples }}', '**', '*.*')),
        *glob.glob(
            os.path.join(
                cwd, '{{ cookiecutter.project.dirs.examples }}', '*.*')),
        *glob.glob(
            os.path.join(
                cwd, 'requirements', '*.*')),
        *glob.glob(
            os.path.join(
                cwd, 'requirements', '**', '*.*')),
        *glob.glob(
            os.path.join(
                cwd, '*.*')),
        *glob.glob(
            os.path.join(
                cwd, '.*'))]
    files = [f for f in files if os.path.isfile(f)]
    for file in sorted(set(files), reverse=True):
        f = FileInfo(file)
        if os.path.exists(f.basename):
            if f.suffix == f.legacy:
                print('    + {}'.format(f.path))
                msgl = '    [ '
                msgr = ' ]'
            elif f.suffix == f.update:
                shutil.move(f.path, f.basename)
                msgl = '    . '
                msgr = ''
            else:
                print('{}{}{}'.format(msgl, f.basename, msgr))
                msgr = ''
                msgl = msgn
        else:
            print('{}{}{}'.format(msgl, f.basename, msgr))
            shutil.move(f.path, f.basename)
            msgr = ''
            msgl = msgn


def delete_dirs():
    """Delete temp dirs."""
    temp_dir = '{{ cookiecutter.project.dirs.temp }}'
    if Path(temp_dir).exists():
        shutil.rmtree(temp_dir)


def main():
    """Create/Update files and folders in project folder."""
    delete_dirs()
    rename_files()


if __name__ == '__main__':
    main()
