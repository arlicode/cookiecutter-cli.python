# -*- coding: utf-8 -*-
# {{ cookiecutter.project.license.copyright.o }}
# {{ cookiecutter.project.license.copyright.l }}
"""Starts project folder setup."""
import re
import sys


# Validate project name
project_name = '{{ cookiecutter.project.slug }}'
name_regex = re.compile(r'^[\_\-a-zA-Z][\_\-a-zA-Z0-9]+$')
if not name_regex.match(project_name):
    print('ERROR: {} is not a valid Python module name!'.format(project_name))
    sys.exit(1)

# Validate email
email = '{{ cookiecutter.author_email }}'
email_regex = re.compile(r'''
    (^[-!#$%&'*+/=?^_`{}|~0-9A-Z]+
    (\.[-!#$%&'*+/=?^_`{}|~0-9A-Z]+)*
    |^"([\001-\010\013\014\016-\037!#-\[\]-\177]
    |\\[\001-011\013\014\016-\177])*
    )@(?:[A-Z0-9](?:[A-Z0-9-]{0,61}[A-Z0-9])?\.)+
    [A-Z]{2,6}\.?$''', re.VERBOSE | re.IGNORECASE)
if not email_regex.search(email):
    print('Error: {} is not a valid email!'.format(email))
    sys.exit(1)

# Validate command name
cmd = '{{ cookiecutter.project.command }}'
cmd_regex = re.compile('[^a-zA-Z0-9-_]')
if cmd_regex.search(cmd):
    print('Error: {} is not a command name!'.format(cmd))
    sys.exit(1)
